/**
 * Fibonacci.
 * The {@code Fibonacci}.
 *
 * @author Sergii Fomin
 * @version 1.0 Build Nov 8, 2019
 */

import java.util.Scanner;

public class EntryPoint {

    /**
     * Here start point of my program
     * @param args command line values
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter first number: ");
        int from = in.nextInt();
        System.out.print("Enter Last number: ");
        int to = in.nextInt();
        System.out.print("Enter Fibonacci sequence size: ");
        int size = in.nextInt();

        /**
         * Calls method for odd numbers printing
         */
        printOddNumbers(from, to);
        /**
         * Calls method for even numbers printing
         */
        printEvenNumbers(from, to);
        /**
         * Calls method for sum of odd and even numbers printing
         */
        printSumOfNumbers(from, to);
        /**
         * Call method for creating of Fibonacci sequence
         */
        buildFibonacciNumbers(size);
    }

    /**
     * Method for printing of Odd Fibonacci numbers
     *
     * @return list of Fibonacci sequence
     */
    private static void printOddNumbers(int from, int to) {
        /**
         * Prints odd numbers
         */
        for (int i = from; i <= to; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }
    }

    /**
     * Method for printing of Even Fibonacci numbers
     *
     * @return list of Fibonacci sequence
     */
    private static void printEvenNumbers(int from, int to) {
        System.out.println("Even numbers from end to start:");
        /**
         * Prints even numbers
         */
        for (int i = to; i >= from; i--) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }

    /**
     * Method for printing Sum of Fibonacci numbers
     *
     * @return list of Fibonacci sequence
     */
    private static void printSumOfNumbers(int from, int to) {
        int oddNumbersSum = 0;
        int evenNumbersSum = 0;

        /**
         * Counts sum of odd and even numbers
         */
        for (int i = from; i <= to; i++) {
            if (i % 2 != 0) {
                oddNumbersSum = oddNumbersSum + i;
            } else {
                evenNumbersSum = evenNumbersSum + i;
            }
        }
        System.out.println("Sum of odd numbers: " + oddNumbersSum);
        System.out.println("Sum of even numbers: " + evenNumbersSum);
    }

    /**
     * Method for finding Fibonacci numbers
     *
     * @return list of Fibonacci sequence
     */
    private static void buildFibonacciNumbers(int size) {
        int firstNumber = 0,
                secondNumber = 1,
                sum;
        int biggestOddNumber = 0,
                biggestEvenNumber = 0;
        int oddNumbersCount = 1,
                evenNumbersCount = 1,
                fibonacciNumberCount = size + 1;

        System.out.println("Fibonacci sequence:");

        /**
         * Printing first number
         */
        System.out.println(firstNumber);

        /**
         * Printing second number
         */
        System.out.println(secondNumber);

        /**
         * Counting Fibonacci numbers
         */
        for (int i = 1; i < size; i++) {
            sum = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = sum;

            /**
             * Printing Fibonacci sequence
             */
            System.out.println(sum);

            /**
             * Finding biggest odd number
             */
            if (sum % 2 != 0) {
                biggestOddNumber = sum;
                oddNumbersCount++;
            } else {
                biggestEvenNumber = sum;
                evenNumbersCount++;
            }
        }

        /**
         * Counting odd numbers percentage
         */
        double oddNumbersPercentage = ((double) oddNumbersCount / fibonacciNumberCount) * 100;

        /**
         * Counting even numbers percentage
         */
        double evenNumbersPercentage = ((double) evenNumbersCount / fibonacciNumberCount) * 100;

        /**
         * Printing biggest odd number Fibonacci sequence
         * @param biggestOddNumber  - the biggest odd number
         * @param biggestEvenNumber - the biggest even number
         */
        System.out.println("Biggest odd number in Fibonacci sequence F1 = " + biggestOddNumber);
        System.out.println("Biggest even number in Fibonacci sequence F2 = " + biggestEvenNumber);

        /**
         * Calculating percentage of odd numbers
         * @return percentage of odd numbers
         */
        System.out.println("Percentage of odd numbers in Fibonacci sequence: " + oddNumbersPercentage + " %");

        /**
         * Calculating percentage of even numbers
         * @return percentage of even numbers
         */
        System.out.println("Percentage of odd numbers in Fibonacci sequence: " + evenNumbersPercentage + " %");
    }
}
